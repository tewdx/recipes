# Engelsaugen

## Zutaten

* 240g Mehl
* 150g Butter
* 2 Eigelb
* 70g Puderzucker
* 1 Paeckchen Vanillezucker
* 1 Spritzer Zitronensaft
* 1 Prise Salz
* Konfituere

## Zubereitung

Alle Zutaten mit den Knethaken zu einem glatten Teig verkneten und dann fuer 1-2 Stunden kalt stellen. Aus dem Teig Kugeln je 15g formen und auf einem mit Backpapier belegten Backblech verteilen. Mit einem bemehlten Kochloeffel oder aehnlichem Mulden in die Kugeln druecken und diese mit der Konfituere fuellen. Im auf 180 Grad Celsius vorgeheizten Backofen 10-15 min backen.
