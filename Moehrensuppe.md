# Moehrensuppe

## Zutaten

* 1kg Moehren
* 1kg Kartoffeln
* ca 2l Gemuesebruehe
* ca 300g Mettenden
* Butter
* Petersilie

## Zubereitung

Moehren schaelen und in duenne Scheiben schneiden. Im Topf mit soviel Gemuesebruehe uebergiessen, dass sie schwimmen. Mettenden mehrmals mit einer Gabel anstechen und im Ganzen in den Topf. Moehren aufkochen und koecheln lassen.

Kartoffeln schaelen und grob schneiden und mit Bruehe bissfest kochen. Wasser abgiessen, Kartoffeln nach belieben zerkleinern (Messer ein paar Mal duch den Topf ziehen, stampfen, ...) und zu den Moehren geben. 

Alles etwa 1h koecheln lassen. Butter und Petersilie je nach Geschmack dazu (mehr schmeckt mehr).

Vor dem Essen die Mettenden herausnehmen, in Scheiben schneiden und wieder in den Topf geben.
